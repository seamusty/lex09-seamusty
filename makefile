src = %.c 

include $(sources:.c=.d)

OBJDIR := objdir
obj := $(OBJDIR)/%.o : %.c
	$(COMPILE.c) $(OUTPUT_OPTION $<) 

all: $(obj) 

$(obj): | $(OBJDIR)

$(OBJDIR):
	mkdir $(OBJDIR)

.PHONY : clean
clean : 
	rm interpreter $(objects)
